package com.example.vaadinspringbootdemo.component;

import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

public class customMenuBar extends HorizontalLayout {
    public customMenuBar(){

        /*
        Anchor link = new Anchor();
        link.setHref("/room");
        link.setText("Room");
        add(link);

        Anchor link2 = new Anchor();
        link2.setHref("/rooms");
        link2.setText("Rooms");
        add(link2);

        Anchor link3 = new Anchor();
        link3.setHref("/course");
        link3.setText("Course");
        add(link3);


        Anchor link4 = new Anchor();
        link4.setHref("/courses");
        link4.setText("Courses");
        add(link4);

        */
         AddMenuItem("/room", "Room");
         AddMenuItem("/rooms", "Rooms");
         AddMenuItem("/course", "Course");
         AddMenuItem("/courses", "Courses");
    }

    public void AddMenuItem(String href, String text){
        Anchor anchor = new Anchor();
        anchor.setHref(href);
        anchor.setText(text);
        add(anchor);
    }
}
