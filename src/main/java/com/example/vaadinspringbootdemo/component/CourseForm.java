package com.example.vaadinspringbootdemo.component;
import com.example.vaadinspringbootdemo.entity.Terem;
import com.example.vaadinspringbootdemo.repository.KurzusRepository;
import com.example.vaadinspringbootdemo.entity.Kurzus;
import com.example.vaadinspringbootdemo.repository.TeremRepository;
import com.example.vaadinspringbootdemo.view.Reloader;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.awt.*;

@SpringComponent
@UIScope
public class CourseForm extends VerticalLayout {
    @Autowired
    private KurzusRepository courserepository;
    @Autowired
    private TeremRepository teremrepository;
    private Kurzus course;
    private Reloader reloader;
    private TextField name;
    private Binder<Kurzus> binder;
    private ComboBox<Terem> terem;

    public Kurzus getCourse() {
        return course;
    }

    public void setCourse(Kurzus course) {
        this.course = course;
        binder.setBean(this.course);
    }

    public Reloader getReload() {
        return reloader;
    }

    public void setReload(Reloader reloader) {
        this.reloader = reloader;
    }


    @PostConstruct
    public void init() {
        terem = new ComboBox<>();
        terem.setLabel("Terem");
        try{
            terem.setItems(teremrepository.findAll());
            terem.setItemLabelGenerator(Terem::getName);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        binder = new Binder<>(Kurzus.class);
        name = new TextField("Name");

        Button deleteButton = new Button();
        deleteButton.setText("Delete");
        deleteButton.setIcon(VaadinIcon.TRASH.create());
        deleteButton.addClickListener(buttonClickEvent -> {
            courserepository.delete(course);
            reloader.reload();
            Notification.show("Succesfull remove!");
            setVisible(false);
        });

        Button editButton = new Button();
        editButton.setText("Save");
        editButton.setIcon(VaadinIcon.PENCIL.create());
        editButton.addClickListener(buttonClickEvent -> {
            courserepository.update(course);
            reloader.reload();
            Notification.show("Succesful edit!");
            setVisible(false);
        });

        Button viewPage = new Button("About");
        viewPage.addClickListener(e -> {
            UI.getCurrent().navigate("kurzus/view/"+course.getId());
        });
        add(viewPage);


        add(name);
        add(terem);
        add(deleteButton);
        setVisible(false);
        add(editButton);
        setVisible(false);
        binder.bindInstanceFields(this);
    }
}
