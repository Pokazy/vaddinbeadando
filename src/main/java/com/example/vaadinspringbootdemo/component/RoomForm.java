package com.example.vaadinspringbootdemo.component;

import com.example.vaadinspringbootdemo.repository.TeremRepository;
import com.example.vaadinspringbootdemo.entity.Terem;
import com.example.vaadinspringbootdemo.view.Reloader;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.awt.*;

@SpringComponent
@UIScope
public class RoomForm extends VerticalLayout {
    @Autowired
    private TeremRepository roomrepository;
    private Terem room;
    private Reloader reloader;
    private TextField name;
    private Binder<Terem> binder;

    public Terem getRoom() {
        return room;
    }

    public void setRoom(Terem room) {
        this.room = room;
        binder.setBean(this.room);
    }

    public Reloader getReload() {
        return reloader;
    }

    public void setReload(Reloader reloader) {
        this.reloader = reloader;
    }


    @PostConstruct
    public void init(){
        binder = new Binder<>(Terem.class);
        name = new TextField("Name");
        Button deleteButton = new Button();
        deleteButton.setText("Delete");
        deleteButton.setIcon(VaadinIcon.TRASH.create());
        deleteButton.addClickListener(buttonClickEvent -> {
            roomrepository.delete(room);
            reloader.reload();
            Notification.show("Succesfull remove!");
            setVisible(false);
        });

        Button editButton = new Button();
        editButton.setText("Save");
        editButton.setIcon(VaadinIcon.PENCIL.create());
        editButton.addClickListener(buttonClickEvent -> {
            roomrepository.update(room);
            reloader.reload();
            Notification.show("Succesful edit!");
            setVisible(false);
        });

        Button viewPage = new Button("About");
        viewPage.addClickListener(e -> {
            UI.getCurrent().navigate("terem/view/"+room.getId());
        });
        add(viewPage);

        add(name);
        add(deleteButton);
        setVisible(false);
        add(editButton);
        setVisible(false);
        binder.bindInstanceFields(this);


    }


}
