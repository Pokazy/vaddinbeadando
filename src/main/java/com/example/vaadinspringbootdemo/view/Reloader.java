package com.example.vaadinspringbootdemo.view;

public interface Reloader {
    public void reload();
}
