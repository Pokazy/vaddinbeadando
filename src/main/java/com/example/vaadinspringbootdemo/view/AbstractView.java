package com.example.vaadinspringbootdemo.view;

import com.example.vaadinspringbootdemo.component.customMenuBar;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public abstract class AbstractView extends VerticalLayout {
    public void addMenuBar(){
        add(new customMenuBar());
    }
}
