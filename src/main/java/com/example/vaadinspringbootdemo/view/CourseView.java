package com.example.vaadinspringbootdemo.view;
import com.example.vaadinspringbootdemo.view.AbstractView;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.router.Route;

@Route
public class CourseView extends AbstractView {

    public CourseView(){
        addMenuBar();
        add(new Text("Ez egy kurzus képernyője"));
    }
}
