package com.example.vaadinspringbootdemo.view;
import com.example.vaadinspringbootdemo.component.CourseForm;
import com.example.vaadinspringbootdemo.entity.Kurzus;
import com.example.vaadinspringbootdemo.repository.KurzusRepository;
import com.example.vaadinspringbootdemo.view.AbstractView;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.List;

@Route
public class CoursesView extends AbstractView implements Reloader{
    // sql pv: 123
    @Autowired
    private KurzusRepository repository;
    @Autowired
    private CourseForm form;
    Grid<Kurzus> grid;
    @PostConstruct
    public void init(){
        addMenuBar();
        List<Kurzus> list = repository.findAll(); //10 elemü lista, for ciklussal egy terem object beadása.
        if(list.isEmpty()) {
            for (int i = 0; i < 10; i++) {
                Kurzus k = new Kurzus();
                //k.setRoom(i);
                k.setName("Kurzus neve " + i);
                repository.save(k);
            }
            list = repository.findAll();
        }

        grid = new Grid<>();  //új táblázat létrehozás
        grid.setItems(list); // lista betöltése
        grid.addColumn(Kurzus::getName).setHeader("Név"); //csinál magának egy akkora ciklust mint a táblázat, végigjárja a be adott dolgot.
        grid.addColumn(Kurzus::getId).setHeader("Id");
        grid.addColumn(Kurzus::getRoom).setHeader("Terem");
        grid.asSingleSelect().addValueChangeListener(gridRoomComponentValueChangeEvent -> {
            form.setVisible(true);
            form.setCourse(gridRoomComponentValueChangeEvent.getValue());
            form.setReload(this);
        });


        Button button = new Button();
        button.setText("Add");
        button.setIcon(VaadinIcon.PLUS.create());
        button.addClickListener(buttonClickEvent -> {
            form.setVisible(true);
            form.setReload(this);
            form.setCourse(new Kurzus());
        });
        add(button);
        add(grid);
        add(form);
    }

    @Override
    public void reload() {
        grid.setItems(repository.findAll());
    }
}
