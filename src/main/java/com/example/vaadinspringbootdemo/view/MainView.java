package com.example.vaadinspringbootdemo.view;

import com.example.vaadinspringbootdemo.view.AbstractView;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.router.Route;

@Route  //import
public class MainView extends AbstractView {
    public MainView(){
        addMenuBar();
        Button btn = new Button();  // gomb létrehozás
        btn.setText("Kattolj meg");  //szöveg beállítás
        btn.addClickListener(buttonClickEvent -> Notification.show("Amafa")); // onclick eventre a Notification osztály show metódusa kiírja a paraméterbe beírt cuccot.
        add(btn);  //hozzáadás a vertikális kéeprnyőhöz ()extends vertical layout miatt

        Button btn2 = new Button();
        btn2.setText("Körtefa");
        btn.addClickListener(buttonClickEvent -> Notification.show("Körte"));
        add(btn2);

    }
}
