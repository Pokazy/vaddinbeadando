package com.example.vaadinspringbootdemo.view;

import com.example.vaadinspringbootdemo.view.AbstractView;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.router.Route;

@Route
public class RoomView extends AbstractView {


    public RoomView(){ //elérése urlben RoomView -> room
        addMenuBar();
        add(new Text("Ez egy terem képernyője"));  // sima kiiratás
    }
}
