package com.example.vaadinspringbootdemo.view;

import com.example.vaadinspringbootdemo.entity.Kurzus;
import com.example.vaadinspringbootdemo.entity.Terem;
import com.example.vaadinspringbootdemo.repository.KurzusRepository;
import com.example.vaadinspringbootdemo.repository.TeremRepository;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.html.ListItem;
import com.vaadin.flow.component.html.OrderedList;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

@Route(value = "terem/view")
public class TeremPageView extends VerticalLayout implements HasUrlParameter<String> {

    @Autowired
    private TeremRepository repository;
    @Autowired
    private KurzusRepository kurzusRepository;

    @Override
    public void setParameter(BeforeEvent beforeEvent, String s) {
        try {
            Terem t = repository.findById(Integer.parseInt(s));

            add(new Label("terem: " + t.getName()));
            Grid<Kurzus> grid=new Grid<>();
            grid.setItems(kurzusRepository.findAllByTeremId(t.getId()));
            grid.addColumn(Kurzus::getId).setHeader("Id");
            grid.addColumn(Kurzus::getName).setHeader("Name");
            grid.addColumn(Kurzus::getRoom).setHeader("Room");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}