package com.example.vaadinspringbootdemo.view;

import com.example.vaadinspringbootdemo.entity.Kurzus;
import com.example.vaadinspringbootdemo.repository.KurzusRepository;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

@Route(value = "kurzus/view")
public class KurzusPageView extends VerticalLayout implements HasUrlParameter<String> {

    @Autowired
    private KurzusRepository repository;

    @Override
    public void setParameter(BeforeEvent beforeEvent, String s) {
        try {
            Kurzus k = repository.findById(Integer.parseInt(s));
            if(k.getRoom()!=null){
                add(new Label("Kurzusok: "+k.getRoom().getName()));
            }
            add(new Label("Név: "+k.getName()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
