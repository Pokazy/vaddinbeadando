package com.example.vaadinspringbootdemo.view;

import com.example.vaadinspringbootdemo.component.RoomForm;
import com.example.vaadinspringbootdemo.entity.Terem;
import com.example.vaadinspringbootdemo.repository.TeremRepository;
import com.example.vaadinspringbootdemo.view.AbstractView;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.List;

@Route
public class RoomsView extends AbstractView implements Reloader{
// sql pv: 123
    @Autowired
    private TeremRepository repository;
    @Autowired
    private RoomForm form;
    Grid<Terem> grid;
    @PostConstruct
    public void init(){
        addMenuBar();
        List<Terem> list = repository.findAll(); //10 elemü lista, for ciklussal egy terem object beadása.
        if(list.isEmpty()) {
            for (int i = 0; i < 10; i++) {
                Terem t = new Terem();
                t.setLevel(i);
                t.setName("Terem neve " + i);
                repository.save(t);
            }
            list = repository.findAll();
        }

        grid = new Grid<>();  //új táblázat létrehozás
        grid.setItems(list); // lista betöltése
        grid.addColumn(Terem::getName).setHeader("Név"); //csinál magának egy akkora ciklust mint a táblázat, végigjárja a be adott dolgot.
        grid.addColumn(Terem::getId).setHeader("Id");
        grid.addColumn(Terem::getLevel).setHeader("szint");
        grid.asSingleSelect().addValueChangeListener(gridRoomComponentValueChangeEvent -> {
                form.setVisible(true);
                form.setRoom(gridRoomComponentValueChangeEvent.getValue());
                form.setReload(this);
                });
        //kommrnt
        Button button = new Button();
        button.setText("Add");
        button.setIcon(VaadinIcon.PLUS.create());
        button.addClickListener(buttonClickEvent -> {
            form.setVisible(true);
            form.setReload(this);
            form.setRoom(new Terem());
        });
        add(button);
        add(grid);
        add(form);
    }

    @Override
    public void reload() {
        grid.setItems(repository.findAll());
    }
}
