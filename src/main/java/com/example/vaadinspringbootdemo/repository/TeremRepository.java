package com.example.vaadinspringbootdemo.repository;

import com.example.vaadinspringbootdemo.entity.Terem;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public class TeremRepository {
    @PersistenceContext // az appplication properties alapján összeköti a projektet a db-vel
    private EntityManager entityManager;

    public void save(Terem room){
        entityManager.persist(room); //mentés db-be

    }
    public List<Terem> findAll(){
        return entityManager.createNamedQuery(Terem.FIND_ALL).getResultList();
    }
    public Terem findById(int id){
        return entityManager.find(Terem.class, id);

    }
    public void delete(Terem room){
        entityManager.remove(findById(room.getId()));
    }
    public void update(Terem room){
        entityManager.merge(room);
    }

}
