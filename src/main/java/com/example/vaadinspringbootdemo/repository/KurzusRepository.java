package com.example.vaadinspringbootdemo.repository;
import com.example.vaadinspringbootdemo.entity.Kurzus;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public class KurzusRepository {
    @PersistenceContext
    private EntityManager entityManager;

    public void save(Kurzus course){
        entityManager.persist(course);

    }
    public List<Kurzus> findAll(){
        return entityManager.createNamedQuery(Kurzus.FIND_ALL).getResultList();
    }
    public List<Kurzus> findAllByTeremId(Integer id){
        Query q = entityManager.createNamedQuery(Kurzus.FIND_BY_TEREM_ID);
        q.setParameter("teremId", id);
        return q.getResultList();

    }
    public Kurzus findById(int id){
        return entityManager.find(Kurzus.class, id);
    }
    public void delete(Kurzus course){
        entityManager.remove(findById(course.getId()));
    }
    public void update(Kurzus course){
        entityManager.merge(course);
    }



}
