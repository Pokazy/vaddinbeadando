package com.example.vaadinspringbootdemo.entity;

import javax.persistence.*;
@NamedQuery(name = Terem.FIND_ALL, query = "select n from Terem n")

@Entity
@Table
public class Terem {
    public static final String FIND_ALL = "Terem.findAll";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private int level;

    public Terem(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
