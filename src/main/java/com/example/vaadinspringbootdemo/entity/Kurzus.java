package com.example.vaadinspringbootdemo.entity;

import javax.persistence.*;
@NamedQueries({
        @NamedQuery(name = Kurzus.FIND_ALL, query = "select n from Kurzus n"),
        @NamedQuery(name = Kurzus.FIND_BY_TEREM_ID, query = "select n from Kurzus n where n.terem.id=:teremId")

})

@Entity
@Table
public class Kurzus {
    public static final String FIND_ALL = "Kurzus.findAll";
    public static final String FIND_BY_TEREM_ID = "Kurzus.findAllByTeremId";


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    @ManyToOne
    @JoinColumn(name="room_id")
    private Terem terem;


    public Kurzus(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Terem getRoom() {
        return terem;
    }

    public void setRoom(Terem terem) {
        this.terem = terem;
    }
}
